import React, { Component } from 'react';
import { AppRouter } from './AppRouter';
import { store } from './store';

//import styles
import './styles/App.css';

class App extends Component {

  loading() {
    return new Promise(resolve => setTimeout(resolve, 3000))
  }

  componentDidMount() {
    // устанавливаем фоновое изображение, которое может меняться из настроек
    document.body.style.backgroundImage = 'url(/images/backgrounds/' + store.getState().background_file_name + '.jpg)';
    this.loading().then(
      result => {
        // убираем прелоадер приложения если всё ок
        const preloader_app = document.getElementById('preloader-app');
        preloader_app.classList.add('hidden');
        preloader_app.style.display = "none";
      },
      error => {
        console.error('ERROR')
      }
    )
  }

  render() {
    return (
      <AppRouter />
    );
  }
}

export default App;
