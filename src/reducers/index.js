export default (state, action) => {
    switch (action.type) {
        case "SET_NOTEBOOK_TEXT":
            return {
                ...state,
                notebook_text: action.text,
            };
        case "SET_BACKGROUND":
            return {
                ...state,
                background_file_name: action.fileName
            };
        case "SET_LOGIN":
            return {
                ...state,
                login: action.login
            };
        default:
            return state
    }
};