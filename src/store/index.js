import { createStore } from 'redux';
import reducer from '../reducers';
import { loadState, saveState } from './localStorage';

const prevState = loadState();

const initState = {
    notebook_text: " ",
    background_file_name: "main",
    login: "React"
};

export const store = createStore(reducer, (prevState === undefined) ? initState : prevState);

store.subscribe(() => {
    saveState({
        notebook_text: store.getState().notebook_text,
        background_file_name: store.getState().background_file_name,
        login: store.getState().login,
    })
})