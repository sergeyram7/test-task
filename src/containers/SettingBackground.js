import React from 'react';
import ImgBackground from '../components/ImgBackground';
import '../styles/Setting.css';

export class SettingBackground extends React.Component {
    constructor(props){
        super(props);
        this.arrayBackgroundName = [
            'main',
            'alt02',
            'alt03',
            'alt04',
        ]
    }

    render() {
        return (
            <section>
                <h3>Изменить фоновое изображение</h3>
                <div className="row-backgrounds">
                    {this.arrayBackgroundName.map((backgroundName, index) => {
                        return <ImgBackground key={index} fileName={backgroundName}/>
                    })}
                </div>
            </section>
        )
    }
}