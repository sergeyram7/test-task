import React from 'react';
import { store } from '../store';
import { setBackground } from '../actions';

export default class ImgBackground extends React.Component {

    setBackground(e){
        const fileName = e.target.dataset.name;
        document.body.style.backgroundImage = 'url(/images/backgrounds/' + fileName + '.jpg)';
        store.dispatch(setBackground(fileName));
    }

    render() {
        return (
            <div className="img-background" 
            data-name={this.props.fileName} 
            onClick={this.setBackground}
            style={{backgroundImage: 'url(/images/backgrounds/' + this.props.fileName + '.jpg)'}}>
            </div>
        )
    }
}