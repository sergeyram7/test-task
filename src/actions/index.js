export function setNotebookText(text) {
    return {
        type: "SET_NOTEBOOK_TEXT",
        text: text
    }
}

export function setBackground(fileName) {
    return {
        type: "SET_BACKGROUND",
        fileName: fileName
    }
}

export function setLogin(login) {
    return {
        type: "SET_LOGIN",
        login: login
    }
}