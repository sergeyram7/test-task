import React from 'react';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';

// import screen
import { IndexScreen as Index } from './screen/Index';
import { SettingsScreen as Settings } from './screen/Settings';
import { AuthorScreen as Author } from  './screen/Author';

export class AppRouter extends React.Component {
    render() {
        return (
            <Router>
                <main>
                    <div id="preloader-screen" style={{display: 'none'}}>
                        <div className="loader-anim"></div>
                    </div>
                    <aside>
                        <nav>
                            <ul>
                                <li>
                                    <NavLink to="/" exact activeClassName="selected">Блокнот</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/author/" activeClassName="selected">Автор</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/settings/" activeClassName="selected">Настройки</NavLink>
                                </li>
                                <li>
                                    {this.props.location}
                                </li>
                            </ul>
                        </nav>
                    </aside>
                    <section className="content">
                        <Route path="/" exact component={Index}/>
                        <Route path="/author/" component={Author}/>
                        <Route path="/settings/" component={Settings}/>
                    </section>
                </main>
            </Router>
        )
    }
}