import React from 'react';
import { store } from '../store';
import { setLogin } from '../actions';

export class AuthorScreen extends React.Component {

    loading() {
        return new Promise(resolve => {
            setTimeout(resolve, 3000);
        })
    }

    componentDidMount() {
        // включаем прелоудер при подключении компонента
        const preloader_screen = document.getElementById('preloader-screen');
        preloader_screen.style.display = "block";
        this.loading().then(
            result => {
                // при успешной загрузке отключаем прелоудер
                preloader_screen.style.display = "none";
            },
            error => {
                console.error('ERROR')
            }
        )
    }

    onLoginChange(e) {
        store.dispatch(setLogin(e.target.value));
    }

    render() {
        return (
            <section>
                <h2>Автор</h2>
                <p>
                    <label>
                        Логин:
                        <input type="text"
                            name="login"
                            defaultValue={store.getState().login}
                            onChange={this.onLoginChange} />
                    </label>
                </p>
            </section>

        )
    }
}