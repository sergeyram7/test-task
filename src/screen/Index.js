import React from 'react';
import { store } from '../store';
import { setNotebookText } from '../actions';

export class IndexScreen extends React.Component {

    loading() {
        return new Promise(resolve => {           
            // устанавливаем значение блокнота
            document.getElementById('notebook').value = store.getState().notebook_text;
            setTimeout(resolve, 3000);
        }) 
    }

    componentDidMount() {
        // включаем прелоудер при подключении компонента
        const preloader_screen = document.getElementById('preloader-screen');
        preloader_screen.style.display = "block";
        this.loading().then(
            result => {
                // при успешной загрузке отключаем прелоудер
                preloader_screen.style.display = "none";
            },
            error => {
                console.error('ERROR')
            }
        )
    }

    // componentWillUnmount() {
    //     // сохраняем значение блокнота при размонтировании компонента
    //     let notebook = document.getElementById('notebook');
    //     let notebook_text = notebook.value;
    //     store.dispatch(setNotebookText(notebook_text));
    // }

    saveNotebookText() {
        store.dispatch(setNotebookText(document.getElementById('notebook').value));
    }

    render() {
        return (
            <section>
                <div>
                    <h2>Блокнот</h2>
                    <textarea id="notebook" onChange={this.saveNotebookText}></textarea>
                    <p>Автор: {store.getState().login}</p>
                </div>
            </section>
        )
    }
}