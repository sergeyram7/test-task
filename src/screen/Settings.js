import React from 'react';
import { SettingBackground } from '../containers/SettingBackground';

export class SettingsScreen extends React.Component {
    loading() {
        return new Promise(resolve => {
            setTimeout(resolve, 3000);
        }) 
    }

    componentDidMount() {
        // включаем прелоудер при подключении компонента
        const preloader_screen = document.getElementById('preloader-screen');
        preloader_screen.style.display = "block";
        this.loading().then(
            result => {
                // при успешной загрузке отключаем прелоудер
                preloader_screen.style.display = "none";
            },
            error => {
                console.error('ERROR')
            }
        )
    }

    render() {
        return (
            <section>
                <h2>Настройки</h2>
                <SettingBackground/>
            </section>
        )
    }
}